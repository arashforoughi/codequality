#!/bin/sh

# Make it TAP compliant, see http://testanything.org/tap-specification.html
echo "1..14"

failed=0
step=1

temp_path="$PWD/test/tmp"
fixtures_path="$PWD/test/fixtures"
got_file="$temp_path/gl-code-quality-report.json"
expected_file="test/expect/gl-code-quality-report.json"
got_file_html="$temp_path/gl-code-quality-report.html"
expected_file_html="test/expect/gl-code-quality-report.html"

setup() {
  # Copy needed fixture files to a temp src directory
  mkdir $temp_path
  cp "$fixtures_path/argument_count.rb" $temp_path
}

teardown() {
  # Delete temp directory to avoid conflict with succeeding test runs
  rm -rf $temp_path
}

run_cc() {
  DEFAULT_FILES_PATH="$PWD/codeclimate_defaults" SOURCE_CODE="$temp_path" ./run.sh "$temp_path"
}

run_cc_stdout() {
  REPORT_STDOUT=1 DEFAULT_FILES_PATH="$PWD/codeclimate_defaults" SOURCE_CODE="$temp_path" ./run.sh "$temp_path"
}

run_cc_html() {
  REPORT_FORMAT=html DEFAULT_FILES_PATH="$PWD/codeclimate_defaults" SOURCE_CODE="$temp_path" ./run.sh "$temp_path"
}

run_cc_html_stdout() {
  REPORT_FORMAT=html REPORT_STDOUT=1 DEFAULT_FILES_PATH="$PWD/codeclimate_defaults" SOURCE_CODE="$temp_path" ./run.sh "$temp_path"
}

desc="Exit with error message when SOURCE_CODE is not set"
setup
err=$(./run.sh $temp_path)

if [ "$err" == "SOURCE_CODE env variable not set" ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
teardown
step=$((step+1))
echo

# Normal execution
desc="Send expected output to a file"
setup
run_cc

if test $? -eq 0 && diff "$got_file" "$expected_file"; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo
teardown

# Detecting proper eslint channel
package_json_path="$temp_path/package.json"
codeclimate_yml_path="$temp_path/.codeclimate.yml"

test_eslint_channel() {
  dependency_type=$1
  eslint_version=$2
  eslint_channel=$3

  setup
  sed "s/_eslint_version_/$eslint_version/g" "$fixtures_path/package-eslint-in-$dependency_type.json" > $package_json_path

  run_cc

  # First, make sure the command runs successfully first
  if test $? -eq 0 && diff "$got_file" "$expected_file"; then
    # Next, make sure we actually set the correct dependency in package.json fixture
    if grep -q "\"eslint\": \"$eslint_version\"" $package_json_path; then
      # Lastly, check that .codeclimate.yml actually used the correct eslint channel
      if grep -q "channel: \"$eslint_channel\"" $codeclimate_yml_path; then
        echo "ok $step - $desc"
      else
        echo "not ok $step - $desc (.codeclimate.yml does not contain the correct eslint channel, $eslint_channel)"
        failed=$((failed+1))
      fi
    else
      echo "not ok $step - $desc (package.json does not contain the correct eslint version, $eslint_version)"
      failed=$((failed+1))
    fi
  else
    echo "not ok $step - $desc (cc command failed)"
    failed=$((failed+1))
  fi
  step=$((step+1))
  echo
  teardown
}

desc="Use proper channel for eslint version in package.json dependencies (x.x.x notation)"
test_eslint_channel "dependencies" "5.3.0" "eslint-5"

desc="Use proper channel for eslint version in package.json dependencies (~x.x.x notation)"
test_eslint_channel "dependencies" "~6.3.0" "eslint-6"

desc="Use proper channel for eslint version in package.json dependencies (^x.x.x notation)"
test_eslint_channel "dependencies" "^3.2.0" "eslint-3"

desc="Use proper channel for eslint version in package.json dependencies (default to stable if major version is greater than 8)"
test_eslint_channel "dependencies" "9.0.0" "stable"

desc="Use proper channel for eslint version in package.json devDependencies (x.x.x notation)"
test_eslint_channel "devDependencies" "5.3.0" "eslint-5"

desc="Use proper channel for eslint version in package.json devDependencies (~x.x.x notation)"
test_eslint_channel "devDependencies" "~6.3.0" "eslint-6"

desc="Use proper channel for eslint version in package.json devDependencies (^x.x.x notation)"
test_eslint_channel "devDependencies" "^3.2.0" "eslint-3"

desc="Use proper channel for eslint version in package.json devDependencies (default to stable if major version is greater than 8)"
test_eslint_channel "devDependencies" "9.0.0" "stable"

# with defined REPORT_STDOUT
desc="Send expected output to STDOUT"
setup

if test $? -eq 0 && run_cc_stdout | diff - "$expected_file"; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo
teardown

# with defined REPORT_FORMAT=html
desc="Generate HTML report"
setup

if test $? -eq 0 && run_cc_html && diff "$got_file_html" "$expected_file_html"; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo
teardown

# with defined REPORT_FORMAT=html and REPORT_STDOUT
desc="Send expected HTML output to STDOUT"
setup

if test $? -eq 0 && run_cc_html_stdout | diff - "$expected_file_html"; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
step=$((step+1))
echo
teardown

desc="Exit with invalid REPORT_FORMAT set"
setup
err=$(REPORT_FORMAT=foobarbaz DEFAULT_FILES_PATH="$PWD/codeclimate_defaults" SOURCE_CODE="$temp_path" ./run.sh $temp_path)

if [ "$err" == "Invalid REPORT_FORMAT value. Must be one of: json|html" ]; then
  echo "ok $step - $desc"
else
  echo "not ok $step - $desc"
  failed=$((failed+1))
fi
teardown
step=$((step+1))
echo

# Finish tests
count=$((step-1))
if [ $failed -ne 0 ]; then
  echo "Failed $failed/$count tests"
  exit 1
else
  echo "Passed $count tests"
fi
