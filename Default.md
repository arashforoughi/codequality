## What does this MR do?

<!-- Briefly described the result of your changes -->

## Are there any open issues relevant to this change?

<!-- https://gitlab.com/gitlab-org/ci-cd/codequality/-/issues -->

## Checklist

- [ ] CI is passing
- [ ] There is a new CHANGELOG entry under `master (unreleased)` that includes a link to this merge request
- [ ] The VERSION file is not modified
